const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: ['./app/main.js'],
    output: {
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test:/\.js|jsx$/,
                exclude:/node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            }
        ]
    },
    devServer: {
        port: 3000
    },
    plugins:[
        new HtmlWebpackPlugin({
           template: './index.html'
        })
     ]
};