const USERS = 'https://jsonplaceholder.typicode.com/users';
const COMM = 'https://jsonplaceholder.typicode.com/comments';
const POST = 'https://jsonplaceholder.typicode.com/posts';

class FistClass {
    constructor(elementId) {
        this.elementId = elementId
      }

      /** FETCH **/
      // getData(uri){
      //   fetch(uri)
      //   .then((response) => { console.log(response); return response.json()})
      //   .then((data)=>{
      //       console.log('ds' ,data);
      //   })
      // }

      /** ASYNC AWAIT **/
      async getDataAsync(uri){
          const response = await fetch(uri);
          const json = await response.json()
          console.log('got a response', json)
      }

}


var app = new FistClass('root');

 //app.getData(USERS);
// console.log("BBBBBB");

app.getDataAsync(USERS);
app.getDataAsync(COMM);
// console.log("DDDDDDD");
